package com.pig4cloud.pig.codegen.service;

import com.pig4cloud.pig.codegen.entity.GenConfig;
import org.junit.Test;

import javax.annotation.Resource;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author leiruiqi
 **/
public class SysGeneratorServiceTest extends BaseServiceTest {

	@Resource
	private SysGeneratorService sysGeneratorService;

	@Test
	public void genTest() throws IOException {
		GenConfig genConfig = new GenConfig();
		genConfig.setAuthor("leiruiqi");
		genConfig.setComments("xxx");
		genConfig.setModuleName("a");
		genConfig.setPackageName("com.test");
		genConfig.setTableName("account");
		byte[] content = sysGeneratorService.generatorCode(genConfig);

		String projectPath = System.getProperty("user.dir");
		//String path = "/Users/macbook/";
		FileOutputStream fos = new FileOutputStream(projectPath+"/test.zip");

		fos.write(content);
		fos.close();
	}
}
